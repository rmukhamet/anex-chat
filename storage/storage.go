package storage

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/rmukhamet/anex-chat/models"
)

// Storage provides an interface to a data storage
type Storage interface {
	UserCheckLogin(string, string) (*models.User, error)
	UserCreate(*models.User) (uuid.UUID, error)
	UserRead(uuid.UUID) (*models.User, error)
	UserUpdate(*models.User) error

	NewTx() (*sqlx.Tx, error)

	Close()
}
