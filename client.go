// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/rmukhamet/anex-chat/models"
	"gitlab.com/rmukhamet/anex-chat/utils"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
	msg     models.Message
	rawMsg  models.RawMsg
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// TokenAuth is a jwt that should be send the very first from a Client to authenticate
type TokenAuth struct {
	Token string `json:"token,omitempty"`
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// header to read userdata
	user models.User

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

// authenticate check Client connections for allowability
func (c *Client) authenticate() {
	var (
		err     error
		message []byte
	)
	defer func() {
		if err != nil {
			c.conn.Close()
		}
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	_, message, err = c.conn.ReadMessage()
	if err != nil {
		if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
			log.Printf("error: %v", err)
		}
		return
	}
	token := &TokenAuth{}
	err = json.Unmarshal(message, token)
	if err != nil {
		log.Println("Received message: ", message)
		log.Println(string(message))
		log.Println("Unmarshal: ", err)
		c.conn.WriteMessage(websocket.TextMessage, []byte("{\"error\":\"Malformed message: a token had been expected\"}"))
		return
	}

	c.user, err = getUserByToken(token.Token)
	log.Println(c.user)

	// if err == nil {
	// 	m := <-c.send // make channel empty
	// 	log.Println(m)
	// }

	c.hub.register <- c
	go c.writePump()
	go c.readPump()
	log.Println("end of auth")
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
// logic here should be here
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, ms, err := c.conn.ReadMessage()
		utils.LogErr(err)
		log.Println(string(ms))
		if err != nil {
			log.Println("read err", err)
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		if strings.HasPrefix(string(ms), "@") {
			msgSlice := strings.SplitAfterN(string(ms), " ", 2)
			targetUser := strings.TrimSpace(strings.TrimLeft(msgSlice[0], "@"))
			log.Println(targetUser)
			log.Println(msgSlice[1])
			msg = models.InitTextMsg(string(ms), c.user.Nickname, targetUser)
		} else {
			msg = models.InitTextMsg(string(ms), c.user.Nickname, "")
		}

		log.Println(msg)
		c.hub.broadcast <- msg

	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				log.Println("something went wrong", string(message))
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				log.Println(err)
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if utils.LogErr(err) {
		return
	}
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
	client.hub.register <- client

	client.authenticate()
}
