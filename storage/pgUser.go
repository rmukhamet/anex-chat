package storage

import (
	"fmt"
	"log"

	"github.com/google/uuid"
	"gitlab.com/rmukhamet/anex-chat/models"
	"gitlab.com/rmukhamet/anex-chat/utils"
	"golang.org/x/crypto/bcrypt"
)

var userFields = `
	user_id,
	username,
	password,
	nickname
`

var userValues = `
	:user_id,
	:username,
	:password,
	:nickname
`

// UserCheckLogin zen comment
func (p *PgsqlDB) UserCheckLogin(password string, username string) (user *models.User, err error) {
	user = &models.User{}

	err = p.db.Get(user, "SELECT * FROM users WHERE username=$1", username)

	if utils.LogErr(err, username) {
		return
	}

	// Comparing the password with the hash
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	utils.LogErr(err, username)

	return
}

// UserCreate zen comment
func (p *PgsqlDB) UserCreate(user *models.User) (id uuid.UUID, err error) {

	var hash []byte

	// Generate "hash" to store from user password
	hash, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if utils.LogErr(err) {
		return
	}
	user.Password = string(hash)

	result, err := p.db.NamedQuery(fmt.Sprintf(`INSERT INTO users (%s) VALUES (%s) RETURNING user_id`, userFields, userValues), user)

	if utils.LogErr(err) {
		log.Println(err)
		return
	}

	m := make(map[string]interface{})

	for result.Next() {
		err = result.MapScan(m)
		if !utils.LogErr(err, m, user) {
			return
		}
	}

	result.Close()

	if len(m) > 0 {
		id, err = uuid.ParseBytes(m["user_id"].([]byte))
		user.ID = id
	}
	utils.LogErr(err)

	return
}

// UserRead returns the User with requested ID
func (p *PgsqlDB) UserRead(userID uuid.UUID) (user *models.User, err error) {
	user = &models.User{}
	err = p.db.Get(user, "SELECT * FROM users WHERE user_id=$1 LIMIT 1", userID)
	utils.LogErr(err)
	return
}

// UserUpdate zen comment
func (p *PgsqlDB) UserUpdate(newUser *models.User) (err error) {

	if newUser.Password != "" {

		var hash []byte

		// Generate "hash" to store from user password
		hash, err = bcrypt.GenerateFromPassword([]byte(newUser.Password), bcrypt.DefaultCost)
		if utils.LogErr(err) {
			return
		}
		newUser.Password = string(hash)

	}

	query := fmt.Sprintf("UPDATE users SET (%s) = (%s) where user_id = :user_id RETURNING user_id", userFields, userValues)
	result, err := p.db.NamedQuery(query, newUser)

	if utils.LogErr(err) {
		//err = errors.New(fmt.Sprint("Error update user:", errSql))
		return
	}

	result.Close()
	return err
}
