-- Пользователь
CREATE TABLE users (
    user_id uuid DEFAULT uuid_generate_v1() PRIMARY KEY,
    username text UNIQUE NOT NULL,
    nickname text UNIQUE NOT NULL,
    password text NOT NULL
);

CREATE INDEX ON users (username);

COMMENT ON TABLE users IS 'Пользователи';

