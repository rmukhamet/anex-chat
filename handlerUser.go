package main

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
	"gitlab.com/rmukhamet/anex-chat/models"
	"gitlab.com/rmukhamet/anex-chat/utils"
)

// register
// login
// change

// CreateUser creates user
func CreateUser(w http.ResponseWriter, r *http.Request) {

	var (
		user models.User
		err  error
	)
	// render response
	defer func() {
		render.Render(w, r, MakeResp(user, err, http.StatusCreated))
	}()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println(string(body))
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println(err)
		return
	}

	user, err = utils.HashPass(user)

	db := env.storage

	utils.LogErr(err)

	userID, err := db.UserCreate(&user)
	utils.LogErr(err, userID)
}

// GetUser takes id of user and returns that user if exists
func GetUser(w http.ResponseWriter, r *http.Request) {
	var (
		user *models.User
		err  error
	)

	// render response
	defer func() {
		render.Render(w, r, MakeResp(user, err, http.StatusOK))
	}()
	db := env.storage
	idStr := chi.URLParam(r, "userID")
	recordID, err := uuid.Parse(idStr)
	utils.LogErr(err)

	user, err = db.UserRead(recordID)
	utils.LogErr(err)
}

// UpdateUser updates user record
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	var (
		user models.User
		err  error
	)

	// render response
	defer func() {
		render.Render(w, r, MakeResp(user, err, http.StatusOK))
	}()

	db := env.storage

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println(string(body))
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println(err)
		return
	}

	user, err = utils.HashPass(user)
	utils.LogErr(err)

	err = db.UserUpdate(&user)
	utils.LogErr(err)
}

func personGetToken(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)

	type loginForm struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	log.Println("got here for jwt")

	var lft loginForm

	err := decoder.Decode(&lft)
	if err != nil {
		render.Render(w, r, ErrInvalidRequest("Invalid json string", err.Error()))
		return
	}
	defer r.Body.Close()

	var (
		user  *models.User
		login *JwtResponse
	)

	log.Println("data:", lft.Password, lft.Username)

	user, err = env.storage.UserCheckLogin(lft.Password, lft.Username)

	if err != nil {
		log.Println("Unauthorized 1:", err)
		render.Render(w, r, ErrUnauthorized(errors.New("Unauthorized : 1")))
		return
	}

	AuthClaim := &JwtClaim{
		IP: r.RemoteAddr,
		ID: user.ID,
	}

	login, err = env.TokenAuth.Encode(AuthClaim)

	utils.LogErr(err)
	log.Println(login)

	ctx := r.Context()
	ctx = context.WithValue(ctx, loginCtxKey{}, login)

	if err != nil {
		render.Render(w, r, ErrUnauthorized(errors.New("Unauthorized : 3")))
		return
	}

	utils.LogErr(err)
	render.Render(w, r.WithContext(ctx), RenderResp(login, 200))
}
