package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/rmukhamet/anex-chat/models"

	"gitlab.com/rmukhamet/anex-chat/utils"

	"github.com/micro/cli"
	"github.com/micro/go-micro/cmd"
	"gitlab.com/rmukhamet/anex-chat/storage"
)

type Env struct {
	storage   storage.Storage
	TokenAuth *JwtAuth
}

var (
	apiAddress     string
	apiTokenKey    string
	env            = &Env{}
	err            error
	GuidNil        = "00000000-0000-0000-0000-000000000000"
	publicMessages = []models.Message{}
	hub            *Hub
)

// DBConfig namespace for db config data
type DBConfig struct {
	User     string
	Password string
	DBname   string
	Host     string
	Port     string
}

func readEnv() DBConfig {
	dbconf := DBConfig{}
	dbconf.User = os.Getenv("DBUSER")
	dbconf.Password = os.Getenv("DBPASS")
	dbconf.DBname = os.Getenv("DBNAME")
	dbconf.Host = os.Getenv("DBHOST")
	dbconf.Port = os.Getenv("DBPORT")

	if dbconf.User == "" { // if envvar for user is empty
		dbconf.User = "cryptolord"
		dbconf.Password = "postgres"
		dbconf.DBname = "cryptomarket"
		dbconf.Host = "cryapi_db"
		dbconf.Port = "5432"
	}

	log.Println(dbconf.User, dbconf.Password, dbconf.DBname, dbconf.Host, dbconf.Port)
	return dbconf
}

func init() {
	flags := cmd.App().Flags
	flags = append(flags, cli.StringFlag{
		Name:        "api_address",
		Value:       "0.0.0.0:80",
		EnvVar:      "API_ADDR",
		Usage:       "Bind API service to address",
		Destination: &apiAddress,
	})
	flags = append(flags, cli.StringFlag{
		Name:        "api_token_key",
		Value:       "somE str!ng",
		EnvVar:      "API_TOKEN_KEY",
		Usage:       "API token key",
		Destination: &apiTokenKey,
	})
	cmd.App().Flags = flags

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	dbNames := readEnv()
	dbNamesStr := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", dbNames.User, dbNames.Password, dbNames.DBname, dbNames.Host, dbNames.Port)
	env.storage, err = storage.NewPgsqlDB(dbNamesStr)
	utils.LogErr(err)
	env.TokenAuth = NewJwtToken("HS256", []byte(apiTokenKey), 1440)

	log.SetOutput(os.Stdout)

}
