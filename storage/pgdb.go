package storage

import (
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// PgsqlDB defines the model for PostgreSQL Data Storage
type PgsqlDB struct {
	db *sqlx.DB
}

// Close closes a connection to DB
func (p *PgsqlDB) Close() {
	p.db.Close()
}

// NewTx starts a new transaction
func (p *PgsqlDB) NewTx() (*sqlx.Tx, error) {
	tx, err := p.db.Beginx()
	return tx, err
}

// NewPgsqlDB establishes a connection to DB and returns an object of Data Storage
func NewPgsqlDB(names string) (*PgsqlDB, error) {
	db, err := sqlx.Connect("postgres", names)
	if err != nil {
		log.Fatalln(err)
	}

	return &PgsqlDB{db: db}, nil
}
