package models

import (
	"github.com/google/uuid"
)

//User model
type User struct {
	ID       uuid.UUID `db:"user_id" json:"id,omitempty"`
	Username string    `db:"username" json:"username"`
	Password string    `db:"password" json:"password,omitempty"`
	Nickname string    `db:"nickname" json:"nickname"`
}
