package main

import (
	"log"
	"net/http"

	"github.com/micro/go-micro/cmd"
	"gitlab.com/rmukhamet/anex-chat/utils"
)

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "home.html")
}

func main() {
	cmd.Init()
	r := Routes()
	hub = newHub()
	go hub.run()

	r.HandleFunc("/", serveHome)
	r.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	err := http.ListenAndServe(apiAddress, r)
	utils.LogErr(err)
}
