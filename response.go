package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
)

// MainResponse split render data in field
type MainResponse struct {
	Auth           *JwtResponse `json:"jwt,omitempty"`
	Data           interface{}  `json:"data,omitempty"`
	RequestID      interface{}  `json:"request_id,omitempty"`
	HTTPStatusCode int          `json:"-"` // http response status code
}

// ErrResponse renderer type for handling all sorts of errors.
//
// In the best case scenario, the excellent github.com/pkg/errors package
// helps reveal information on the error, setting it on Err, and in the Render()
// method, using it to set the application-specific error code in AppCode.
type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string      `json:"errorTxt"`            // user-level status message
	AppCode    int64       `json:"errorCode,omitempty"` // application-specific error code
	ErrorText  string      `json:"error,omitempty"`     // application-level error message, for debugging
	RequestID  interface{} `json:"request_id,omitempty"`
}

// Render base render functionallity for renderer interface
func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

// Render base render functionallity for renderer interface
func (mr *MainResponse) Render(w http.ResponseWriter, r *http.Request) error {
	login := r.Context().Value(loginCtxKey{})
	if login != nil {
		mr.Auth = login.(*JwtResponse)
	}
	render.Status(r, mr.HTTPStatusCode)
	return nil
}

// RenderResp is method that actually takes status code as second arg
func RenderResp(data interface{}, statusCode int) render.Renderer {
	reqid := fmt.Sprintf("request_id: %v", middleware.NextRequestID())
	log.Println(reqid)
	return &MainResponse{
		Data:           data,
		HTTPStatusCode: statusCode,
		RequestID:      reqid,
	}
}

// MakeResp is making respose of 500 if err or given status code and data otherwise
func MakeResp(data interface{}, err error, statusCode int) render.Renderer {
	if err != nil {
		log.Println(err)
		return RenderResp(err, http.StatusInternalServerError)
	} else {
		log.Println("returning response")
		return RenderResp(data, statusCode)
	}
}

// for jwt auth

// ErrUnauthorized 401
func ErrUnauthorized(err error) render.Renderer {
	reqid := fmt.Sprintf("request_id: %v", middleware.NextRequestID())
	log.Println(reqid)
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 401,
		StatusText:     "Unauthorized",
		ErrorText:      err.Error(),
		RequestID:      reqid,
	}
}

// ErrInvalidRequest 400
func ErrInvalidRequest(err string, debugErr string) render.Renderer {

	//TODO Logger
	_ = debugErr

	reqid := fmt.Sprintf("request_id: %v", middleware.NextRequestID())
	log.Println(reqid)

	return &ErrResponse{
		Err:            fmt.Errorf(err),
		HTTPStatusCode: 400,
		StatusText:     "Invalid request",
		ErrorText:      err,
		RequestID:      reqid,
	}
}
