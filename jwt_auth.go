package main

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-chi/render"
	"github.com/google/uuid"
	"gitlab.com/rmukhamet/anex-chat/models"
	"gitlab.com/rmukhamet/anex-chat/utils"
)

// JwtAuth defines the model for a jwt authentication
type JwtAuth struct {
	signKey []byte
	signer  jwt.SigningMethod
	parser  *jwt.Parser
	expTime int64
}

// JwtResponse defines the model for an authenticated response
type JwtResponse struct {
	ID    uuid.UUID    `json:"id,omitempty"`
	Token string       `json:"token,omitempty"`
	User  *models.User `json:"-"`
}

// JwtClaim defines the model for a token claim
type JwtClaim struct {
	IP string
	ID uuid.UUID
	jwt.StandardClaims
}

// loginCtxKey is the context key for the login
type loginCtxKey struct{}

// jwtCtxKey is the context key for the JWT
type jwtCtxKey struct{}

// jwtCtxKey is the context key for JWT errors
type jwtErrCtxKey struct{}

// NewJwtToken creates a JwtAuth authenticator instance that provides middleware handlers
// and encoding/decoding functions for JWT signing.
func NewJwtToken(alg string, signKey []byte, minutes int64) *JwtAuth {
	return &JwtAuth{
		signKey: signKey,
		signer:  jwt.GetSigningMethod(alg),
		expTime: minutes,
	}
}

// Encode creates a token using a custom claims type.  The StandardClaim is embedded
// in the custom type to allow for easy encoding, parsing and validation of standard claims.
func (ja *JwtAuth) Encode(cl *JwtClaim) (login *JwtResponse, err error) {

	login = &JwtResponse{
		ID: cl.ID,
	}

	cl.ExpiresAt = ja.setExpireIn()
	token := jwt.NewWithClaims(ja.signer, cl)

	login.Token, err = token.SignedString(ja.signKey)
	if err != nil {
		log.Fatal("Create Token Error:", err)
	}
	return
}

// Decode creates a token using a custom claims type.  The StandardClaim is embedded
// in the custom type to allow for easy encoding, parsing and validation of standard claims.
func (ja *JwtAuth) Decode(tokenString string) (token *jwt.Token, err error) {

	// sample token is expired.  override time so it parses as valid
	token, err = jwt.ParseWithClaims(tokenString, jwt.MapClaims{}, func(token *jwt.Token) (interface{}, error) {
		return ja.signKey, nil
	})

	return
}

func (ja *JwtAuth) setExpireIn() int64 {
	return time.Now().UTC().Add(time.Minute * time.Duration(ja.expTime)).Unix()
}

// Verifier middleware will verify a JWT passed by a client request.
// The Verifier will look for a JWT token from:
// 1. 'jwt' URI query parameter
// 2. 'Authorization: BEARER T' request header
// 3. Cookie 'jwt' value
//
// The verification processes finishes here and sets the token and
// a error in the request context and calls the next handler.
//
// Make sure to have your own handler following the Validator that
// will check the value of the "jwt" and "jwt.err" in the context
// and respond to the client accordingly. A generic Authenticator
// middleware is provided by this package, that will return a 401
// message for all unverified tokens, see jwtauth.Authenticator.
func (ja *JwtAuth) Verifier(next http.Handler) http.Handler {
	return ja.Verify("")(next)
}

// Verify implements Verifier
func (ja *JwtAuth) Verify(paramAliases ...string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		hfn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			var (
				tokenStr        string
				err             error
				ErrUnauthorized = errors.New("jwtauth: unauthorized token")
				ErrExpired      = errors.New("jwtauth: expired token")
			)

			// Get token from query params
			tokenStr = r.URL.Query().Get("jwt")

			// Get token from other query param aliases
			if tokenStr == "" && paramAliases != nil && len(paramAliases) > 0 {
				for _, p := range paramAliases {
					tokenStr = r.URL.Query().Get(p)
					if tokenStr != "" {
						break
					}
				}
			}

			// Get token from authorization header
			if tokenStr == "" {
				bearer := r.Header.Get("Authorization")
				if len(bearer) > 7 && strings.ToUpper(bearer[0:6]) == "BEARER" {
					tokenStr = bearer[7:]
				}
			}

			// Get token from cookie
			if tokenStr == "" {
				cookie, err := r.Cookie("jwt")
				if err == nil {
					tokenStr = cookie.Value
				}
			}

			// Token is required, cya
			if tokenStr == "" {
				err = ErrUnauthorized
			}

			// Verify the token
			token, err := ja.Decode(tokenStr)
			if err != nil {
				switch err.Error() {
				case "token is expired":
					err = ErrExpired
				}
				ctx = ja.SetContext(ctx, token, err)

				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}

			if token == nil || !token.Valid || token.Method != ja.signer {
				err = ErrUnauthorized
				ctx = ja.SetContext(ctx, token, err)
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}

			// Check expiry via "exp" claim
			if ja.IsExpired(token) {
				err = ErrExpired
				ctx = ja.SetContext(ctx, token, err)

				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}

			// Valid! pass it down the context to an authenticator middleware
			log.Println("valid token")
			ctx = ja.SetContext(ctx, token, err)
			next.ServeHTTP(w, r.WithContext(ctx))

		}

		return http.HandlerFunc(hfn)
	}
}

// SetContext appends values to the context
func (ja *JwtAuth) SetContext(ctx context.Context, t *jwt.Token, err error) context.Context {
	ctx = context.WithValue(ctx, jwtCtxKey{}, t)
	ctx = context.WithValue(ctx, jwtErrCtxKey{}, err)
	return ctx
}

// IsExpired checks that the token is not expired
func (ja *JwtAuth) IsExpired(t *jwt.Token) bool {
	claims := t.Claims.(jwt.MapClaims)

	if expv, ok := claims["exp"]; ok {
		var exp int64
		switch v := expv.(type) {
		case float64:
			exp = int64(v)
		case int64:
			exp = v
		case json.Number:
			exp, _ = v.Int64()
		default:
		}

		if exp < time.Now().UTC().Unix() {
			return true
		}
	}

	return false
}

// Authenticator is a default authentication middleware to enforce access following
// the Verifier middleware. The Authenticator sends a 401 Unauthorized response for
// all unverified tokens and passes the good ones through. It's just fine until you
// decide to write something similar and customize your client response.
func Authenticator(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		if jwtErr, ok := ctx.Value(jwtErrCtxKey{}).(error); ok {
			if jwtErr != nil {
				log.Println(ctx.Value(jwtCtxKey{}))
				log.Println(jwtErr)
				render.Render(w, r, ErrUnauthorized(jwtErr))
				return
			}
		}
		jwtToken, ok := ctx.Value(jwtCtxKey{}).(*jwt.Token)
		if !ok || jwtToken == nil || !jwtToken.Valid {

			render.Render(w, r, ErrUnauthorized(errors.New("Unauthorized : 11")))
			return
		}

		// Check IP in token
		claims := jwtToken.Claims.(jwt.MapClaims)

		// Split on comma.
		ip := strings.Split(claims["IP"].(string), ":")

		ip2 := strings.Split(r.RemoteAddr, ":")

		if ip[0] != ip2[0] {
			log.Printf("Unathorized 12: IP not match  Claim: %s , Exist: %s\n", ip[0], ip2[0])
			render.Render(w, r, ErrUnauthorized(errors.New("Unauthorized : 12")))
			return
		}

		id, err := uuid.Parse(claims["ID"].(string))
		if err != nil {

			render.Render(w, r, ErrUnauthorized(errors.New("Unauthorized : 13")))
			return
		}

		var user *models.User

		user, err = env.storage.UserRead(id)

		login := &JwtResponse{
			ID:    user.ID,
			Token: jwtToken.Raw,
			User:  user,
		}

		ctx = context.WithValue(ctx, loginCtxKey{}, login)

		if err != nil {
			render.Render(w, r, ErrUnauthorized(errors.New("Unauthorized : 14")))
			return
		}

		// Token is authenticated, pass it through
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func getUserByToken(tokenStr string) (models.User, error) {
	user := &models.User{}
	ja := JwtAuth{}

	// Verify the token
	jwToken, err := ja.Decode(tokenStr)
	if utils.LogErr(err) {
		// you cannot die just coz someone sent you bad token
		return *user, err
	}

	if jwToken.Claims == nil {
		log.Println("token has no claims")
		return *user, err
	}

	// Check IP in token
	claims := jwToken.Claims.(jwt.MapClaims)

	id, err := uuid.Parse(claims["ID"].(string))
	if err != nil {

		utils.LogErr(err)
		return *user, err
	}
	user, err = env.storage.UserRead(id)
	utils.LogErr(err)
	return *user, err
}

// LoginInject simply appends login value to a context
func LoginInject(ctx context.Context, login *JwtResponse) context.Context {
	ctx = context.WithValue(ctx, loginCtxKey{}, login)
	return ctx
}

// LoginEject returns login value
func LoginEject(r *http.Request) (*JwtResponse, error) {
	l := r.Context().Value(loginCtxKey{})
	if l == nil {
		return nil, errors.New("login not found in context")
	}
	return l.(*JwtResponse), nil
}
