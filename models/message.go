package models

import (
	"log"
	"time"

	"github.com/google/uuid"
)

// RawMsg for unmarhsal message from front
type RawMsg struct {
	Message string `json:"message"`
	To      string `json:"to"`
}

// Message zen comment
type Message struct {
	ID         uuid.UUID `db:"message_id" json:"id,omitempty"`
	Type       string    `db:"m_type" json:"type"` // nn
	From       string    `json:"from_user"`
	To         string    `json:"to_user"` // user model
	Body       string    `db:"body" json:"body"`
	SendedAt   time.Time `db:"sended_at" json:"sended_at"`
	ReceivedAt time.Time `json:"received_at"`
}

// InitTextMsg fills up fields of a message
func InitTextMsg(text, from, to string) Message {
	msg := Message{}
	log.Println(uuid.New(), "inside inittextmsg")
	msg.ID = uuid.New()
	msg.Type = "text"
	msg.ReceivedAt = time.Now()
	msg.Body = text
	msg.From = from
	msg.To = to

	return msg
}

// Ustamp struct that keeps data about message sender or receiver
type Ustamp struct {
	UserID   uuid.UUID
	Nickname string
}
