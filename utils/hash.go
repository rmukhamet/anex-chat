package utils

import (
	"errors"
	"log"

	"gitlab.com/rmukhamet/anex-chat/models"
	"golang.org/x/crypto/bcrypt"
)

// HashPass takes user struct and changes his string password to a hashed one
func HashPass(user models.User) (models.User, error) {
	if user.Password == "" {
		err := errors.New("user does not have a password, this is not acceptable")
		return user, err
	}

	// Generate "hash" to store from user password
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if LogErr(err) {
		return user, err
	}
	user.Password = string(hash)
	log.Println("hashed password", user.Password)
	return user, nil
}
