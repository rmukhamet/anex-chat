// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab.com/rmukhamet/anex-chat/models"
	"gitlab.com/rmukhamet/anex-chat/utils"
)

// hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan models.Message

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

func newHub() *Hub {
	return &Hub{
		broadcast:  make(chan models.Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			var msgDeliv bool
			log.Println(message)
			for client := range h.clients {
				log.Println(client.user.Nickname, len(client.user.Nickname),
					message.To, len(message.To), client.user.Nickname == message.To)
				if message.To == "" || message.To == client.user.Nickname {

					if message.To == "" {
				//	buf := make([]int, 1, 101)
	//for i := 0; i < 100; i++ {
	//	buf = append(buf, i)
	//	fmt.Printf("Append %d, len: %d , cap: %d \n", i, len(buf), cap(buf))
	}
						publicMessages = append(publicMessages, message)
					}
					byteMsg, err := json.Marshal(fmt.Sprintf("%v: %v", message.From, message.Body))
					utils.LogErr(err)
					select {
					case client.send <- byteMsg:
						log.Println(message)
						msgDeliv = true
					default:
						close(client.send)
						delete(h.clients, client)
					}
				}
			}
			if !msgDeliv {
				for client := range h.clients {
					if message.From == client.user.Nickname {
						byteMsg, err := json.Marshal(fmt.Sprintf("Your message was not delivered. Target user is not online."))
						utils.LogErr(err)
						select {
						case client.send <- byteMsg:
							log.Println(message)
							msgDeliv = true
						default:
							close(client.send)
							delete(h.clients, client)
						}
					}
				}
			}
		}
	}
}
