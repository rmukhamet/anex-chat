package utils

import (
	"log"
	"runtime"
)

// LogErr just checks if err != nil and then logs it, returns true if error isn't nil
func LogErr(e error, otherArgs ...interface{}) (b bool) { // any number of arguments
	_, fn, line, _ := runtime.Caller(1)
	if e != nil {
		log.Printf("%s:%d %v %v", fn, line, e, otherArgs)
		b = true
	}
	return
}
