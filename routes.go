package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/goware/cors"
	"gitlab.com/gitlab.com/rmukhamet/anex-chat/utils"
)

// Routes describes all routes used in the frontend
func Routes() *chi.Mux {

	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.RealIP)
	r.Use(middleware.StripSlashes)

	// Basic CORS
	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(cors.Handler)

	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		code, err := w.Write([]byte("pong"))
		utils.LogErr(err, code)
	})

	// Unprotected route to get jwt token
	r.Group(func(r chi.Router) {
		r.Post("/login", personGetToken)
	})

	// Protected routes
	r.Group(func(r chi.Router) {
		// Seek, verify and validate JWT tokens
		r.Use(env.TokenAuth.Verifier)

		r.Use(Authenticator)

		r.Route("/user", func(r chi.Router) {
			r.Get("/{userID}", GetUser) // read record
			r.Post("/", CreateUser)     // create
			r.Put("/", UpdateUser)      // update and delete (deleting is just updating thru_date)
			//r.Put("/delete", DeleteUser) // puts thru date
		})

		r.Get("/protected", func(w http.ResponseWriter, r *http.Request) {
			code, err := w.Write([]byte("pong if you has been authonticated"))
			utils.LogErr(err, code)
		})

	})

	return r
}
