FROM alpine

ADD . usr/bin/

EXPOSE 80

VOLUME /config

WORKDIR usr/bin/

CMD ["./anex-chat"]